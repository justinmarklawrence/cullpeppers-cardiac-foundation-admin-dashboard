25 mins - Get Firebase working again
10 mins - Logo (look and feel)
05 mins - Look and feel of Add Location Button
03 mins - Filtering
15 mins - Improved visual of location cards
20 mins - Archiving locations
30 mins - Modifying locations
50 mins - Map (just getting it to show up)
30 mins - Google API Autocomplete (when editing and adding locations)
30 mins - Sign in page (look and feel)
40 mins - Sign in page (Firebase authentication)
40 mins - Netlify (domain + hosting)

- Dynamically define heights of main elements

- Image compression
- Logging out
- Modal headings + subheads
- Mobile Design

--- DONE ---

## CANNOT SHOW WITHOUT

- User access (only admins can login)

-- Justin do on own

- Map (getting markers to show on the map)
- Filtering

## COULD SHOW WITHOUT - BUT, THEY WOULD NOTICE

- Messaging when email/pass is wrong
- Favicon

## NICE TO HAVE

- Add location info elements
- Form validation
  - Modifying and/or adding locations
  - Signing in/up
