### GITKRAKEN

1. Go to 'main' branch
2. Click 'Pull' button
3. Create branch using the 'Branch' button
4. Fill out branch name: <Trello ticket number>-<description>

### TRELLO (move ticket to In Progress column)

### VSC

5. Solve solution in code

### GITGRAKEN

6. Stage all changes
7. Commit changes
8. Press 'Push' button
9. Press confirm button

### BITBUCKET

10. Go to Pull Requests tab
11. Press Create
12. Press Create Pull Request (blue button)

### TRELLO (move ticket to Code Review column + assign to Justin)

### Message Justin to review and test changes
