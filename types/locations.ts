export type AEDLocation = {
    id?: string;
    name: string;
    address: string;
    phoneNumber: string;
    archived: boolean;
}

export type AEDLocationWithLatLng = AEDLocation & {lat: number, lng: number};

export type ParsedAddress =  {
    label: string,
    value: {
        description: string;
        matched_substrings: 
            [
                {
                    length: number;
                    offset: number;
                }
            ],
        place_id: string;
        reference: string;
        structured_formatting: {
            main_text: string;
            main_text_matched_substrings: [
                {
                    length: number;
                    offset: number;
                }
            ],
            secondary_text: string;
        },
        terms: [
            {
                offset: number;
                value: string;
            },
            {
                offset: number;
                value: string;
            },
            {
                offset: number;
                value: string;
            },
            {
                offset: number;
                value: string;
            }
        ],
        types: string[]
    }
};